package org.acme.datatypes;

public class Transaction {
    public Integer amount;
    public Integer balance;
    public String creditor;
    public String debtor;
    public String description;
    public Integer id;
    public Integer time;
}
