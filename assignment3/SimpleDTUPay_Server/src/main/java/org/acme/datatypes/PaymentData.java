package org.acme.datatypes;

public class PaymentData {
    public Integer amount;
    public String creditor;
    public String debtor;
    public String description;    
}
