package org.acme;

import io.quarkus.runtime.annotations.QuarkusMain;

import org.acme.storage.Database;

import io.quarkus.runtime.Quarkus;

@QuarkusMain  
public class Main {

    public static void main(String ... args) {
        System.out.println("Server running!");
        Database.setup();
        System.out.println("Running Quarkus!");
        Quarkus.run(args);
        Database.clear();
    }
}