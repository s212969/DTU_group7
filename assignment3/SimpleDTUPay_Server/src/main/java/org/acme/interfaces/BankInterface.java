package org.acme.interfaces;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

public class BankInterface {

	public static BankService bank = new BankServiceService().getBankServicePort();
	private static List<String> usersIds = new ArrayList<String>();

	public static String createAccount(User user, BigDecimal balance) {
		try {
			String id = bank.createAccountWithBalance(user, balance);
			System.out.println("Bank account created for " + user.getFirstName() + " with id " + id);
			usersIds.add(id);
			return id;
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void removeAllAccounts() {
		System.out.println("Bank accounts removed!");
		for (String id : usersIds) {
			try {
				bank.retireAccount(id);
			} catch (BankServiceException_Exception e) {
			}
		}
	}
}
