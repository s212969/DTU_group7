package org.acme.datatypes;

public class User {
   public String cprNumber;
   public String firstName;
   public String lastName;
   
   public dtu.ws.fastmoney.User toFastMoneyUser() {
	   dtu.ws.fastmoney.User user = new dtu.ws.fastmoney.User();
	   user.setCprNumber(cprNumber);
	   user.setFirstName(firstName);
	   user.setLastName(lastName);
	   return user;
   }
}
