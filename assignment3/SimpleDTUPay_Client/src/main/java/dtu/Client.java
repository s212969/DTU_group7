package dtu;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.acme.datatypes.Account;
import org.acme.datatypes.AccountInfo;
import org.acme.datatypes.CreateAccountData;
import org.acme.datatypes.PaymentData;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

import com.google.gson.Gson;

public class Client {
	
	private String URI = "http://0.0.0.0:8080/";
	private RESTApi restApi = new RESTApi();
	private int statusCode = 200;

	public int getStatusCode() {
		return statusCode;
	}
	
	public List<AccountInfo> getAccounts() {
		String URL = URI + "accounts";
		Response response = restApi.request(URL, "GET", false, null);
		statusCode = response.getStatus();
		List<AccountInfo> list = (List<AccountInfo>)response.getEntity();
		return list; 
	}
	
	public String createAccount(CreateAccountData createAccountData) {
		String URL = URI + "accounts";
		Response response = restApi.request(URL, "POST", false, Entity.json(createAccountData));
		statusCode = response.getStatus();
		String id = response.readEntity(String.class);
		return id;
	}
	
	public Account getAccountById(String id) {
		String URL = URI + "accounts/" + id;
		Response response = restApi.request(URL, "GET", false, null);
		statusCode = response.getStatus();
		String json = response.readEntity(String.class);
		System.out.println(json);
		if (response.getStatus() == Response.Status.OK.getStatusCode()) return new Gson().fromJson(json, Account.class);
		else return null;
	}
	
	public String deleteAccountById(String id) {
		String URL = URI + "accounts/" + id;
		Response response = restApi.request(URL, "DELETE", false, null);
		statusCode = response.getStatus();
		String json = response.readEntity(String.class);
		return json;
	}
	
	public String createTransaction(PaymentData payment) {
		String URL = URI + "payments";
		Response response = restApi.request(URL, "POST", false, Entity.json(payment));
		statusCode = response.getStatus();
		String json = response.readEntity(String.class);
		return json;
	}
	
	
}
