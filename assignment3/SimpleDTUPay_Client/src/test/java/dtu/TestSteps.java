package dtu;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.acme.datatypes.CreateAccountData;
import org.acme.datatypes.PaymentData;
import org.acme.datatypes.Transaction;
import org.acme.datatypes.User;
import org.acme.interfaces.BankInterface;


import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.BankServiceException_Exception;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class TestSteps {
	String result;
	Client service = new Client();

	User customer = new User();
	int customerBalance = 0;
	String customerDTUPayID = null;
	String customerBankID = null;
	User merchant = new User();
	int merchantBalance = 0;
	String merchantBankID = null;
	String merchantDTUPayID = null;
	Integer paymentAmount = 0;
	Boolean donePayment = true;
	String errorMessage = "";
	public List<Transaction> paymentsList = new ArrayList<Transaction>();
	
	@Given("the customer {string} {string} with CPR {string} has a bank account with balance {int}")
	public void the_customer_with_cpr_has_a_bank_account_with_balance(String name, String surname, String cpr, Integer amount) {
		customer = new User();
		customer.cprNumber = cpr; 
		customer.firstName = name; 
		customer.lastName = surname;
		customerBalance = amount;
		customerBankID = BankInterface.createAccount(customer.toFastMoneyUser(), BigDecimal.valueOf(amount));
	}

	@Given("that the customer is registered with DTU Pay")
	public void that_the_customer_is_registered_with_dtu_pay() {
		CreateAccountData ca = new CreateAccountData();
		ca.user = customer;
		ca.balance = customerBalance; 
		customerDTUPayID = service.createAccount(ca);
	}

	@Given("a merchant {string} {string} with CPR {string} has a bank account with balance {int}")
	public void a_merchant_with_cpr_has_a_bank_account_with_balance(String name, String surname, String cpr, Integer amount) {
		merchant = new User();
		merchant.cprNumber = cpr; 
		merchant.firstName = name; 
		merchant.lastName = surname;
		merchantBalance = amount;
		merchantBankID = BankInterface.createAccount(merchant.toFastMoneyUser(), BigDecimal.valueOf(amount));
	}

	@Given("that the merchant is registered with DTU Pay")
	public void that_the_merchant_is_registered_with_dtu_pay() {
		CreateAccountData ca = new CreateAccountData();
		ca.user = merchant;
		ca.balance = merchantBalance; 
		merchantDTUPayID = service.createAccount(ca);
	}

	@Then("the balance of the customer at the bank is {int} kr")
	public void the_balance_of_the_customer_at_the_bank_is_kr(Integer balance) {
		try {
			assertEquals(BigDecimal.valueOf(balance), BankInterface.bank.getAccount(customerBankID).getBalance());
		} catch (BankServiceException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("the balance of the merchant at the bank is {int} kr")
	public void the_balance_of_the_merchant_at_the_bank_is_kr(Integer balance) {
		try {
			assertEquals(BankInterface.bank.getAccount(merchantBankID).getBalance(), BigDecimal.valueOf(balance));
		} catch (BankServiceException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@When("the merchant initiates a payment for {int} kr by the customer")
	public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) {
		PaymentData payment = new PaymentData();
		paymentAmount = amount;
		payment.amount = paymentAmount;
		payment.creditor = merchantDTUPayID;
		payment.debtor = customerDTUPayID;
		payment.description = "Bank transfer";
		List<AccountInfo> ai = BankInterface.bank.getAccounts();
		System.out.println("customerBankID: " + customerBankID);
		System.out.println("merchantBankID: " + merchantBankID);
		dtu.ws.fastmoney.Account customerAcc = null;
		dtu.ws.fastmoney.Account merchantAcc = null;
		
		
		errorMessage = service.createTransaction(payment);
		if (service.getStatusCode() == Response.Status.NO_CONTENT.getStatusCode()) {
			donePayment = true;
		} else {
			donePayment = false;
		}
	}

	@Then("the payment is not successful")
	public void the_payment_is_unsuccessful() {
		assertFalse(donePayment);
	}
	
	@Then("the payment is successful")
	public void the_payment_is_successful() {
		assertTrue(donePayment);
	}
	
	@Before
	public void testSetup() {
		BankInterface.removeAllAccounts();
	}
	
	@After
	public void testEnd(){
		BankInterface.removeAllAccounts();
	} 
	
}
