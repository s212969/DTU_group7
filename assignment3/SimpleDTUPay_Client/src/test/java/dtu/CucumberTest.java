package dtu;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.acme.datatypes.Account;
import org.acme.datatypes.CreateAccountData;
import org.acme.datatypes.PaymentData;
import org.acme.datatypes.Transaction;
import org.acme.datatypes.User;
import org.acme.interfaces.BankInterface;
import org.junit.runner.RunWith;

import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.BankServiceException_Exception;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

/* Important: 
for Cucumber tests to be recognized by Maven, the class name has to have
either the word Test in the beginning or at the end. 
For example, the class name CucumberTests (Test with an s) will be ignored by Maven.
*/

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "summary", publish = false, features = "features" // directory of the feature files
		, snippets = SnippetType.CAMELCASE)
public class CucumberTest {
}
