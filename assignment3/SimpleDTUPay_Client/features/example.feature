Feature: Payment

  Scenario: Successful Payment
    Given the customer "XCarlo" "Meroni" with CPR "22042453" has a bank account with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant "XEriks" "Markevics" with CPR "22042454" has a bank account with balance 1000
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is successful
    And the balance of the customer at the bank is 900 kr
    And the balance of the merchant at the bank is 1100 kr

  Scenario: Not enough funds 
    Given the customer "XCarlo" "Meroni" with CPR "22042453" has a bank account with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant "XEriks" "Markevics" with CPR "22042454" has a bank account with balance 1000
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for 1100 kr by the customer
    Then the payment is not successful
   	And the balance of the customer at the bank is 1000 kr
    And the balance of the merchant at the bank is 1000 kr