package dtu;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.acme.datatypes.Account;
import org.acme.datatypes.PaymentData;
import org.acme.datatypes.Transaction;
import org.junit.runner.RunWith;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

/* Important: 
for Cucumber tests to be recognized by Maven, the class name has to have
either the word Test in the beginning or at the end. 
For example, the class name CucumberTests (Test with an s) will be ignored by Maven.
*/

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "summary", publish = false, features = "features" // directory of the feature files
		, snippets = SnippetType.CAMELCASE)
public class CucumberTest {

	String result;
	Client service = new Client();

	String customerID = "";
	String merchantID = "";
	Integer paymentAmount = 0;
	Boolean donePayment = true;
	String errorMessage = "";
	public List<Transaction> paymentsList = new ArrayList<Transaction>();

	@Given("a customer with id {string}")
	public void a_customer_with_id(String id) {
		customerID = id;
	}

	@Given("a merchant with id {string}")
	public void a_merchant_with_id(String id) {
		merchantID = id;
	}

	@Then("the payment is successful")
	public void the_payment_is_successful() {
		assertTrue(donePayment);
	}

	@Given("a successful payment of {int} kr from customer {string} to merchant {string}")
	public void a_successful_payment_of_kr_from_customer_to_merchant(Integer amount, String customerID,
			String merchantID) {
		PaymentData payment = new PaymentData();
		paymentAmount = amount;
		this.customerID = customerID;
		this.merchantID = merchantID;
		payment.amount = paymentAmount;
		payment.creditor = merchantID;
		payment.debtor = customerID;
		payment.description = "";
		errorMessage = service.createTransaction(payment);
		if (service.getStatusCode() == Response.Status.NO_CONTENT.getStatusCode()) {
			donePayment = true;
		} else {
			donePayment = false;
		}
	}

	@When("the manager asks for a list of payments")
	public void the_manager_asks_for_a_list_of_payments() {
		Account a = service.getAccountById(customerID);
		if (a != null) {
			paymentsList = a.transactions;
		}
	}

	@Then("the list contains a payments where customer {string} paid {int} kr to merchant {string}")
	public void the_list_contains_a_payments_where_customer_paid_kr_to_merchant(String customerID, int amount,
			String merchantID) {
		Transaction t = paymentsList.stream().filter(p -> (p.creditor.equals(merchantID) && p.debtor.equals(customerID) && p.amount == amount)).findAny().orElse(null);
		assertTrue(t != null);
	}

	@When("the merchant initiates a payment for {int} kr by the customer")
	public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) {
		PaymentData payment = new PaymentData();
		paymentAmount = amount;
		payment.amount = paymentAmount;
		payment.creditor = merchantID;
		payment.debtor = customerID;
		payment.description = "";
		errorMessage = service.createTransaction(payment);
		if (service.getStatusCode() == Response.Status.NO_CONTENT.getStatusCode()) {
			donePayment = true;
		} else {
			donePayment = false;
		}
	}

	@Then("the payment is not successful")
	public void the_payment_is_not_successful() {
		assertFalse(donePayment);
	}

	@Then("an error message is returned saying {string}")
	public void an_error_message_is_returned_saying(String msg) {
		assertEquals(msg, errorMessage);
	}

}
