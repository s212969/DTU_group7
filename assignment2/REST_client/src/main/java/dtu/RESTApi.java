package dtu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class RESTApi {
	
	public javax.ws.rs.core.Response request(String Url, String requestMethod, Boolean usejson, Entity<?> entity) {
		javax.ws.rs.client.Client client = ClientBuilder.newClient();
		String mediatype = null;
		if (usejson) mediatype = MediaType.TEXT_PLAIN;
		else mediatype = MediaType.APPLICATION_JSON;
		if (requestMethod == "GET") return client.target(Url).request(mediatype).get();
		else if (requestMethod == "POST") return client.target(Url).request(mediatype).post(entity);	
		else if (requestMethod == "PUT") return client.target(Url).request(mediatype).put(entity);
		else if (requestMethod == "DELETE") return client.target(Url).request(mediatype).delete();
		else return null;
	}
	
}
