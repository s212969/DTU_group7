package org.acme.storage;

import java.util.ArrayList;
import java.util.List;

import org.acme.datatypes.Account;
import org.acme.datatypes.AccountInfo;
import org.acme.datatypes.CreateAccountData;
import org.acme.datatypes.Transaction;
import org.acme.datatypes.User;

public class Database {
    public static List<Account> accounts = new ArrayList<Account>();

    public static void addAccount(CreateAccountData createAccount) {
        //add test user
        Account acc = new Account();
        acc.balance = createAccount.balance;
        if (accounts.size() == 0) acc.id = "0";
        else acc.id = Integer.toString(Integer.parseInt(accounts.get(accounts.size()-1).id) + 1);
        acc.transactions = new ArrayList<Transaction>();
        acc.user = new User();
        acc.user = createAccount.user;
        accounts.add(acc);
    }

    public static boolean removeAccount(String id) {
        return accounts.removeIf(a -> id.equals(a.id));
    }

    public static List<AccountInfo> getAccountInfos() {
        List<AccountInfo> a = new ArrayList<AccountInfo>();
        for (Account acc : accounts) {
            AccountInfo ai = new AccountInfo();
            ai.accountId = acc.id;
            ai.user = acc.user;
            a.add(ai);
        }
        return a;
    }

    public static void setup() {
        CreateAccountData a = new CreateAccountData();
        a.balance = 0;
        a.user = new User();
        a.user.cprNumber = "220482343";
        a.user.firstName = "Carlo";
        a.user.lastName = "Meroni";
        addAccount(a);
        
        CreateAccountData b = new CreateAccountData();
        b.balance = 200;
        b.user = new User();
        b.user.cprNumber = "220432343";
        b.user.firstName = "Eriks";
        b.user.lastName = "Markevics";
        addAccount(b);
    }
}