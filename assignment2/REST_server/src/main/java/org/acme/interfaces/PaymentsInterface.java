package org.acme.interfaces;

import java.util.Date;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import org.acme.datatypes.Account;
import org.acme.datatypes.PaymentData;
import org.acme.datatypes.Transaction;
import org.acme.storage.Database;

@Path("/payments")
public class PaymentsInterface {

	public int getTime() {
		return (int) (new Date().getTime() / 1000);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response pay(String json) {
		// return Response.status(Response.Status.BAD_REQUEST).build();
		try {
			PaymentData payment = new Gson().fromJson(json, PaymentData.class);
			Account creditor = Database.accounts.stream().filter(a -> payment.creditor.equals(a.id)).findAny()
					.orElse(null);
			Account debtor = Database.accounts.stream().filter(a -> payment.debtor.equals(a.id)).findAny().orElse(null);

			// check if creditor exists
			if (creditor == null) {
				// 400 Bad request
				System.out.println("POST /payments -> 400 bad request (creditor not found)");
				return Response.status(Response.Status.BAD_REQUEST).entity("Creditor " + payment.creditor + " not found")
						.build();
			}
			// check if debtor exists
			if (debtor == null) {
				// 400 Bad request
				System.out.println("POST /payments -> 400 bad request (debtor not found)");
				return Response.status(Response.Status.BAD_REQUEST).entity("Debtor " + payment.debtor + " not found")
						.build();
			}

			// Fill transaction data
			Transaction t = new Transaction();
			t.creditor = creditor.id;
			t.debtor = debtor.id;
			t.amount = payment.amount;
			t.description = payment.description;
			t.time = getTime();

			// check if debtor can pay
			if (debtor.balance - payment.amount < 0) {
				// 400 Bad request
				System.out.println("POST /payments -> 400 bad request (debtor cannot pay)");
				return Response.status(Response.Status.BAD_REQUEST).entity("Not enough balance to conclude transaction")
						.build();
			}

			// store transaction in both creditor and debtor accounts
			t.balance = debtor.balance - payment.amount;
			debtor.transactions.add(t);
			t.balance = creditor.balance + payment.amount;
			creditor.transactions.add(t);

			// update balance
			debtor.balance -= payment.amount;
			creditor.balance += payment.amount;

			// 201 payment added to db
			System.out.println("POST /payments -> 201 no content");
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (Exception e) {
			// 400 Bad request
			System.out.println("POST /payments -> 400 bad request");
			return Response.status(Response.Status.BAD_REQUEST).entity("Bad request").build();
		}
	}
}
