package org.acme.interfaces;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import org.acme.datatypes.Account;
import org.acme.datatypes.CreateAccountData;
import org.acme.datatypes.ErrorType;
import org.acme.storage.Database;

@Path("/accounts")
public class AccountsInterface {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccounts() {
        System.out.println("GET /accounts -> 200 ok");
        return Response.ok(Database.getAccountInfos()).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAccount(String json) {
        try 
        {
            CreateAccountData accData = new Gson().fromJson(json, CreateAccountData.class);
            Database.addAccount(accData);
        
            // 201 Created
            System.out.println("POST /accounts -> 201 created");
            return Response.status(Response.Status.CREATED).build();
        }
        catch (JsonSyntaxException e) 
        {
            //400 Bad request
            System.out.println("POST /accounts -> 400 bad request");
            return Response.status(Response.Status.BAD_REQUEST).entity("Syntax error in request").build();
        }
        catch (JsonParseException e) 
        {
            //400 Bad request
            System.out.println("POST /accounts -> 400 bad request");
            return Response.status(Response.Status.BAD_REQUEST).entity("Server could not parse request").build();
        }
        catch (Exception e)
        {
            //500 Internal server error
            System.out.println("POST /accounts -> 500 internal server error");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Internal server error").build();
        }
    }

    @Path("/{id}")
    @GET
    public Response getAccountFromID(@PathParam("id") String id) {
        Account acc = Database.accounts.stream().filter(a -> id.equals(a.id)).findAny().orElse(null);

        //200 Account found
        if (acc != null) {
            System.out.println("GET /accounts/" + id + "-> 200 ok");
        	return Response.ok(acc).build();
        }
        //404 Accound not found
        else {
            System.out.println("GET /accounts/" + id + "-> 404 not found");
        	return Response.status(Response.Status.NOT_FOUND).entity(new ErrorType("Error account not found")).build();
        }
    }
    
    @Path("/{id}")
    @DELETE
    public Response remoteAccountByID(@PathParam("id") String id) {
        Boolean removed = Database.removeAccount(id);
        
        //204 Account removed
        if (removed) {
            System.out.println("DELETE /accounts/" + id + "-> 204 no content");
        	return Response.status(Response.Status.NO_CONTENT).build();
        }
        //404 Account not found
        else {
            System.out.println("DELETE /accounts/" + id + "-> 404 not found");
        	return Response.status(Response.Status.NOT_FOUND).entity(new ErrorType("Error account not found")).build();
        }
    }
}
