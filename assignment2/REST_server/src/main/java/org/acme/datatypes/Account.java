package org.acme.datatypes;

import java.util.List;

public class Account {
    public Integer balance;
    public String id;
    public List<Transaction> transactions;
    public User user;
}
